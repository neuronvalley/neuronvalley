# neuron-valley
‘Neuron Valley’ is a ‘not-for-profit’ initiative by [Santulan Ventures](http://santulan.co/). A little contribution towards making Chandigarh region a hub for emerging technologies.

To start with following are some links that might be relevant to a new startup, a business migrating to Chandigarh or opening a local office.

*Gitlab Page: [`https://neuronvalley.gitlab.io/neuronvalley`](https://neuronvalley.gitlab.io/neuronvalley)*

-----------------

## Startup groups on social media
- [Chandigarh Startup Circle](https://www.facebook.com/groups/ChandigarhStartups)

## Investor Networks
- [Chandigarh Angels Network](http://chandigarhangelsnetwork.com/ )

## Support Organizations for Startups
- [TiE Chandigarh](http://chandigarh.tie.org/)
- [SACC](www.saccindia.org)

## Major Events
- [TED Chandigarh](http://www.tedxchandigarh.org/)
- [Startup Jalsa](http://startupjalsa.com)
- [Startup Weekend](http://www.chandigarh.startupweekend.org)

## Co-working spaces

Chandiarh
- [Innov8](https://innov8.work/locations/city-emporium/)
- [SpaceJam](http://www.spacejam.in/)
- [Workcave](http://workcave.in/)
- [Next57](https://www.next57.com/)

Mohali
- [Starthub Nation](https://www.facebook.com/starthubnation/)
- [Startup in a box](http://www.startupinabox.in/)

Panchkula
- [Starthub Nation](https://www.facebook.com/starthubnation/)

## Incubators
- [Santulan Venture Labs](http://santulan.co/)
- [Chitkara innovation incubator](https://www.chitkara.edu.in/chitkara-innovation-incubator/)

## List of Startups and Tech Companies
- [Angel List](https://angel.co/chandigarh)
- [Chrunchbase](https://www.crunchbase.com/location/chandigarh/9e9637697d59484fbf84c5ed0e027a77)

## Jobs in Tech Companies
- [Linkedin](https://www.linkedin.com/jobs/search/?location=Chandigarh) 
- [Naukri](https://www.naukri.com/jobs-in-chandigarh)
- [Angel List](https://angel.co/chandigarh/jobs)
- [Glasdoor](https://www.glassdoor.co.in/Job/chandigarh-jobs-SRCH_IL.0,10_IC2879615.htm)

## Internship in Startups
- [Internshala](http://internshala.com/internships/internship-in-chandigarh)

## Relevant links to Chandigarh on other popular platforms
- [Quora](https://www.quora.com/topic/Startups-in-Chandigarh)
- [Reddit](https://www.reddit.com/r/Chandigarh/)

## Training and Placement officers for Educational institutes
- [Punjab Engineering College](http://pec.ac.in/tnp/message-tpo)
- [University Institute of Engineering and Technology, Panjab University](http://www.uiet.puchd.ac.in/index.php/training-placements/tpo)
- [IIT Ropar](https://www.iitr.ac.in/Placements/pages/index.html)
- [Chitakara University](https://www.chitkara.edu.in/campus-recruitment/)

## Exhibition spaces
- [Community Centres and open spaces owned by Municial Corporation](http://mcchandigarh.gov.in/bookinfo.htm)

## Auditoriums and Halls
- [Indradhanush Auditorium, Panchkula](http://www.communitycentre.huda.org.in/frmListOfProperties.aspx?q=os)
- [Tagore Theatre](http://tagoretheatrechd.org/)

## Conference Facilites and Convention Centres
- [PHD Chamber of Commerce](http://www.phdcci.in/index.php?route=product/allproducts/location&city_id=13&information_id=18)

## Industry organizations
- [CII Chandigarh](http://www.cii.in/chandigarh)
- [PHD Chamber of Commerce](http://www.phdcci.in/)

## Research Institutes
- [Central Scientific Instruments Organisation (CSIO)](http://www.csio.res.in/)
- [Institute of Microbial Technology (IMTECH)](https://www.imtech.res.in/)
- [National Agri-Food Biotechnology Institute (NABI)](http://www.nabi.res.in/)
- [Institute of Statistical, Social and Economic Research (ISSER)](http://isser.edu.gh/)
- [National Institute of Pharmaceutical Education and Research (NIPER)](http://www.niper.nic.in/)
- [Center for Electronics Design & Technology of India (C-DAC Mohali)](https://cdac.in/index.aspx?id=mohali)
- [National Institute of Technical Teachers Training & Research (NITTTR)](http://www.nitttrchd.ac.in/)
- [Postgraduate Institute of Medical Education and Research](http://pgimer.edu.in/)

## Facilities by local govt for startups and tech companies:
- [Policies](http://chdit.gov.in/policies.htm)
- [Entrepreneur Development Centre (EDC) ](http://chdit.gov.in/edc_detail.htm)
- [STPI, Mohali](http://noida.stpi.in/p-mhl)

Help contributing useful links for existing and new startups/tech companies in Chandigarh.

------------------

## License

The content of this site is licensed under the [Creative Commons Attribution 3.0 Unported License](https://creativecommons.org/licenses/by/3.0/).
